package com.kyjg.clothesmanager.service;

import com.kyjg.clothesmanager.entity.Clothes;
import com.kyjg.clothesmanager.repository.ClothesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ClothesService {
    private final ClothesRepository clothesRepository;

    public void setClothes(String clothesType, String clothesBrand, String price) {
        Clothes addData = new Clothes();

        addData.setClothesType(clothesType);
        addData.setClothesBrand(clothesBrand);
        addData.setPrice(price);

        clothesRepository.save(addData);
    }
}
