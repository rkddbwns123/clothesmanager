package com.kyjg.clothesmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Clothes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String clothesType;
    @Column(nullable = false, length = 30)
    private String clothesBrand;
    @Column(nullable = false, length = 20)
    private String price;
}
