package com.kyjg.clothesmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClothesRequest {
    private String clothesType;
    private String clothesBrand;
    private String price;
}
