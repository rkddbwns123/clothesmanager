package com.kyjg.clothesmanager.repository;

import com.kyjg.clothesmanager.entity.Clothes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClothesRepository extends JpaRepository<Clothes,Long> {
}
